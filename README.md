# Project Polygon

YT presentation link: https://www.youtube.com/watch?v=6WB61JCaFxs

**Description**

Early prototype of my top down shooter. The plan is to make a big square-shape arena, with points of interest in each corner (probably a mini-boss). After each corner is cleared, the boss in the middle would be unlocked. Defeating him would unlock „NG+1”, that would significantly increase the difficulty of all enemies and introduce a few new mechanics. After  finishing „NG+1”, an „NG+2” would unlock, then „NG+3”, „NG+4” and so on, possibly to infinity.

The character power would be defined by his Level, that would unlock different upgrades and weapons, maybe some skill trees.

Unfortunetly as it is one of my first projects in Unity, the code structure leaves much to be desired. The idea for architecture is there, but not the execution.

Right now „Project-polygon” is on hold – I have other projects that I want to focus on, but maybe someday I will come back to it.

**Prototype features**
1.	A controllable player character, with three different weapons. You can change between them by pressing 1-3 on keyboard:
- Pistol - medium firerate, medium dmg, low accuracy
- Sniper Rifle - low firerate, high dmg, high accuracy
- Shotgun -low firerate, multiple low dmg shots, low accuracy

Weapon behaviour is achieved by having a base „WeaponAbstract” class, that is inherited by derived classes „WeaponPrefab” (which shoots prefab bullets – Pistol and Shotgun) and „WeaponRaycast” (which shoots raycast-like shots – Sniper Rifle). This was one of my first attempts to use abstracts in Unity in practise, so even though it is not ideal, it gets a job done and with some minor changes it can be a pretty good weapon system.

2.	Four different enemy types:
- Melee - yellow, fast, moving to a player position
- Seeker - red, fast, follows a player and shoots raycast-like weapons, stops when getting too close
- Sniper - black, slow, when in range, starts aiming with laser pointer. When laser hits the player, its shoots out a raycast shot in that direction after 0.5 seconds. Player can dode laser by moving behind an obstacle
- Orbiter – purple, fast, orbiting a player from a safe distance and shoots out slow, prefab bullets

Each enemy has its own movement behaviour, achieved by inheriting from „EnemyAI” class, which contains methods from „A* Pathfinding” library by Sebastian Lague. „EnemyAI” also inherits from an „Actor” class, which covers an actor health, healthbar and an ability to take damage. „PlayerController” class also inherits from it. Just like Weapons, these classes require few changes to be fully optimal.

3.	A* Pathfinding library – a 2D library, used here to achieve pathfinding for enemies. By default, it just moves gameobjects to a chosen target, but I was able to alter its behaviour for each enemy. For example my Melee enemy moves straight to players position after creating a path, wheareas Sniper enemies move close enough to get into range, and the stop to take aim. Pathfinding shown in a presentation video. 

4.	Three levels, which define the type (prefab) and number of enemies in each level. After every enemy from a level is killed, the next level is loaded. After every level is done, the game is over.
Each level is a scriptable objects which needs to be provided to a list in the GameManager gameobject in the scene, thus making this system highly customizable, with ability to add, remove and edit levels on the fly.

![alt text](Assets/Screenshots/5.PNG "Level SO")
![alt text](Assets/Screenshots/6.PNG "dagdsa")

Screenshots: 

<img src="Assets/Screenshots/1.png"  width="720">
<img src="Assets/Screenshots/2.png"  width="720">
<img src="Assets/Screenshots/3.png"  width="720">

(Most fun you can have - spawn 50 of every enemy type!)
