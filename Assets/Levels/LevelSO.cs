﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Level", menuName = "Level")]
public class LevelSO : ScriptableObject
{
    public List<EnemyType> enemies;

    public int GetNumberOfAllEnemies()
    {
        int sum = 0;

        foreach(var enemy in enemies)
        {
            sum += enemy.numberOfEnemies;
        }

        return sum;
    }

    public void SetEnemiesNumber()
    {
        foreach(var enemy in enemies)
        {
            enemy.InitializeEnemyType();
        }
    }
}
