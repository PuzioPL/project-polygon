﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Actor : MonoBehaviour
{
    public float mult;
    protected Rigidbody2D rig2d;
    public Vector2 clampMagnitude;

    public int maxHealth;
    public int currentHealth;

    public HealthBar healthBar;

    private bool isInvincible = false;
    public float invincibilityTime = 1f;

    void Awake()
    {
        rig2d = GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    protected void Move(Vector2 dir)
    {
        // PONIZEJ PORUSZANIE ZROBIONE NA RIGIDBODY2D.ADDFORCE() I CHYBA DZIAŁA FAJNIE
        // robie na AddForce() a nie na .velocity, bo chce mieć możliwość nakładania "impulsu" kiedy gracz otrzyma obrażenia
        // clamp magnitude robi tak, że ruch po przekątnej nie jest ze zdwojoną prędkością

        var horizontalMovement = transform.right * dir.x;
        var verticalMovement = transform.up * dir.y;

        clampMagnitude = Vector2.ClampMagnitude(horizontalMovement + verticalMovement, 1.0f) * mult * Time.deltaTime;

        rig2d.AddForce(clampMagnitude, ForceMode2D.Force);
    }

    public void TakeDamage(Vector2 dir, int damage, bool isPlayer = false)
    {
        if (!isInvincible)
        {
            rig2d.AddForce(dir, ForceMode2D.Impulse);
            var endHealth = currentHealth - damage;
            if (endHealth > maxHealth)
            {
                endHealth = maxHealth;
            }

            // currentHealth > 0 - dirty zabezpieczenie na wypadek kiedy kilka pocisków uderza w przeciwnika NARAZ, kiedy przeciwnik po pierwszym już umrze. 
            // Wtedy każdy kolejny pocisk będzie się liczył jako kolejny pokonany przeciwnik i spawner się psuje. TODO: LOW PRIO
            if (endHealth <= 0 && currentHealth > 0)
            {
                KillActor();

                // jak jest enemy, wywołuj event dla GameManager i UIManager -> zmiana koncepcji, GameManager.instance wystarczy chyba
                if (!isPlayer)
                {
                    GameManager.instance.EnemyKilled();
                }
            }

            currentHealth = endHealth;
            healthBar.SetHealth(currentHealth);
        }

        if (isPlayer && !isInvincible)
        {
            StartCoroutine(TurnInvincible());
        }
    }

    IEnumerator TurnInvincible()
    {
        isInvincible = true;
        yield return new WaitForSeconds(invincibilityTime);
        isInvincible = false;
    }

    public void KillActor()
    {
        Destroy(gameObject);
    }
}
