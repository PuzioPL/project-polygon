﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{
    public Camera cam;
    public float speed = 20.0f;

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = cam.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        //dałem -90, zeby pasowało do sprite (przod postaci zeby celowal w myszke)
        //bez tego postac bedzie obrocona bokiem do kursora, a nie wiem jak obrocic sprite kek


        //transform.rotation = rotation;

        // To do obracania playera z wygładzeniem. Wygląda lepiej niż sztywne obracanie, ale powoduje mniejszą celność przy dużym poruszaniu myszką
        // (ponieważ FirePoint też się obraca z opóźnieniem)
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);
    }
}
