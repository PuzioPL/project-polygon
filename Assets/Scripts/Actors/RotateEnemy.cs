﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Chyba można wcisnąć od razu do EnemyAI -> maybe l8er
/// </summary>
public class RotateEnemy : MonoBehaviour
{
    public Transform target;
    public float speed = 20.0f;
    public bool isRotate = true;
    private Vector3 directionTarget;

    private void Start()
    {
        target = FindObjectOfType<PlayerController>().gameObject.transform;
    }
    private void Update()
    {
        if (isRotate)
        {
            directionTarget = target.position - transform.position;
        }

        Aim((Vector2)directionTarget);
    }
    void Aim(Vector2 direction)
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        //dałem -90, zeby pasowało do sprite (przod postaci zeby celowal w myszke)
        //bez tego postac bedzie obrocona bokiem do kursora, a nie wiem jak obrocic sprite kek

        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);
    }
}
