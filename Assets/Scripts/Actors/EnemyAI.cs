﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAI : Actor
{
    public Transform target;

    // co ile wyznaczana będzie kolejna ścieżka
    public float updatePathInterval = 0.2f;
    public float meleeImpactForce = 7f;
    public int damage = 1;

    // przy jakiej odległości będzie brany kolejny waypoint
    public float nextWaypointDistance = 3f;

    // w jakiej odległości od celu enemy ma się zatrzymać
    public float endReachedDistance = 0.7f;

    protected Path path;
    protected int currentWayPoint = 0;
    protected float distance;
    protected Seeker seeker;
    protected Rigidbody2D rb;
    protected RotateEnemy rotateEnemy;

    protected void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        target = FindObjectOfType<PlayerController>().gameObject.transform;

        InvokeRepeating("UpdatePath", 0f, updatePathInterval);
        seeker.StartPath(rb.position, target.position, OnPathComplete);

        // bierze pierwszego child (czyli Sprite) na ktorym jest skrypt RotateEnemy. Potrzebuje przy np. sniperze
        rotateEnemy = transform.GetChild(0).gameObject.GetComponent<RotateEnemy>();
    }

    void UpdatePath()
    {
        if (seeker.IsDone())
            seeker.StartPath(rb.position, target.position, OnPathComplete);
    }
    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWayPoint = 0;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            var playerActor = collision.gameObject.GetComponent<Actor>();

            var damageDirection = (Vector2)(collision.transform.position - transform.position);

            playerActor.TakeDamage(damageDirection.normalized * meleeImpactForce, damage, true);
        }
    }

    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, nextWaypointDistance);
    }
    */
}
