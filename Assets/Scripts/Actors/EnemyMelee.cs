﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMelee : EnemyAI
{
    void FixedUpdate()
    {
        if (path == null)
            return;

        distance = Vector2.Distance(rb.position, path.vectorPath[currentWayPoint]);

        if (distance > endReachedDistance)
        {
            var direction = ((Vector2)path.vectorPath[currentWayPoint] - rb.position).normalized;
            Move(direction);
        }

        // && currentWayPoint < path.vectorPath.Count - 1 -> potrzebne, ponieważ bez tego enemy nie osiągnie końca ścieżki
        //  bez tego nextWaypointDistance działa jak endReachedDistance
        if (distance < nextWaypointDistance && currentWayPoint < path.vectorPath.Count - 1)
        {
            currentWayPoint++;
        }
    }
}
