﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOrbiter : EnemyAI
{
    // odległość z której enemy bedzie orbitować i strzelać dookoła Playera
    public float distanceToOrbit = 5;

    public float distanceToShoot = 5;

    public WeaponAbstract weapon;

    public float strifeSpeed;

    private void FixedUpdate()
    {
        // powiem tak - łatwo
        // na razie wystarczy - można dodać A* kiedy idzie na początku do playera 
        //  kiedy już do niego dotrze to A* ma być wyłączone - zepsułby wszystko gdyby player wyszedł z zasięgu enemy
        float distance = Vector2.Distance(rb.position, target.position);
        Vector2 direction = ((Vector2)target.position - rb.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        Vector2 newPos;

        // w zasięgu wystrzału
        if (distance < distanceToShoot)
        {
            weapon.Shoot(gameObject, false);
        }

        if (distance > distanceToOrbit)
        {
            newPos = (Vector2)transform.position + direction * Time.fixedDeltaTime * mult;
        }
        else
        {
            // DZIAŁA!!!
            Quaternion rotation = Quaternion.Euler(0, 0, angle);
            Vector3 myVector = transform.right;
            Vector3 rotateVector = rotation * myVector;

            newPos = transform.position + rotateVector * Time.fixedDeltaTime * strifeSpeed;
        }
        newPos -= rb.position;

        rb.AddForce(newPos, ForceMode2D.Force);

        // todo: kiedy krąży na skraju distanceToOrbit, to co chwilę minimalnie się przysuwa w strone playera ponieważ distance na jedną klatkę jest większe od distanceToOrbit 
        // przy niewielkich prędkościach prawie tego nie widać więc to nie jest mega problem, ale kiedyś można popatrzeć na to
    }

    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, distanceToOrbit);
    }
    */
}
