﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class EnemySeeker: EnemyAI
{
    public float distanceToShoot = 10f;

    public float distanceToTarget;

    public WeaponAbstract weapon;

    void FixedUpdate()
    {
        if (path == null)
            return;

        distanceToTarget = Vector2.Distance(rb.position, target.position);

        distance = Vector2.Distance(rb.position, path.vectorPath[currentWayPoint]);

        // może strzelać
        if (distanceToTarget < distanceToShoot)
        {
            weapon.Shoot(gameObject, false);
        }
        
        if (distanceToTarget > endReachedDistance)
        {
            var direction = ((Vector2)path.vectorPath[currentWayPoint] - rb.position).normalized;
            Move(direction);
        }

        // && currentWayPoint < path.vectorPath.Count - 1 -> potrzebne, ponieważ bez tego enemy nie osiągnie końca ścieżki
        //  bez tego nextWaypointDistance działa jak endReachedDistance
        if (distance < nextWaypointDistance && currentWayPoint < path.vectorPath.Count - 1)
        {
            currentWayPoint++;
        }
    }

    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, endReachedDistance);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, nextWaypointDistance);
    }
    */
}