﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class EnemySniper : EnemyAI
{
    public float distanceToShoot = 10f;

    public float distanceToTarget;

    public WeaponAbstract weapon;

    // time before making a shot, after confirming enemy position
    public float fireLeeway = 2f;

    public SniperState state = SniperState.Moving;

    public enum SniperState
    {
        Moving = 0,
        Aiming = 1,
        FirePositionConfirmed = 2,
        None = 3
    }

    private void Update()
    {
        if (state == SniperState.Aiming)
        {
            if (weapon.DrawIndicator())
            {
                state = SniperState.FirePositionConfirmed;
            }
        }

        if (state == SniperState.FirePositionConfirmed)
        {
            StartCoroutine(WaitAndShoot());
            state = SniperState.None;
        }

        if (state == SniperState.None)
        {
            weapon.DrawIndicator();
            rotateEnemy.isRotate = false;
        }
    }

    void FixedUpdate()
    {
        if (path == null)
            return;

        distanceToTarget = Vector2.Distance(rb.position, target.position);

        // może strzelać
        if (distanceToTarget < distanceToShoot && state == SniperState.Moving && Time.time >= weapon.nextTimeToFire)
        {
            //isAiming = true;
            state = SniperState.Aiming;
            //StartCoroutine(AimAndShoot());
        }


        if (state == SniperState.Moving)
        {
            if (distanceToTarget > endReachedDistance)
            {
                var direction = ((Vector2)path.vectorPath[currentWayPoint] - rb.position).normalized;
                Move(direction);
            }

            distance = Vector2.Distance(rb.position, path.vectorPath[currentWayPoint]);

            // && currentWayPoint < path.vectorPath.Count - 1 -> potrzebne, ponieważ bez tego enemy nie osiągnie końca ścieżki
            //  bez tego nextWaypointDistance działa jak endReachedDistance
            if (distance < nextWaypointDistance && currentWayPoint < path.vectorPath.Count - 1)
            {
                currentWayPoint++;
            }
        }
    }

    IEnumerator WaitAndShoot()
    {
        // dorób żeby indicator "flashował" przed strzałem ze zwiększającą się częstotliwością
        Color red = new Color(255, 0, 0);
        weapon.SetAimIndicatorColor(red, red);
        yield return new WaitForSeconds(fireLeeway);

        weapon.Shoot(gameObject, false);
        state = SniperState.Moving;
        rotateEnemy.isRotate = true;
    }

    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, endReachedDistance);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, nextWaypointDistance);
    }
    */
}
