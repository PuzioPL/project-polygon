﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Actor
{
    public static Vector2 Position;

    private float hor2d;
    private float ver2d;

    void Start()
    {
        //rig2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        hor2d = Input.GetAxis("Horizontal");
        ver2d = Input.GetAxis("Vertical");
        Move(new Vector2(hor2d, ver2d));

        /*
        // jumping
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("Wcisniecie spacji");
            rig2d.AddForce(new Vector2(0, 30), ForceMode2D.Impulse);
        }
        */
        Position = rig2d.position;
    }
}
