﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPrefabs : WeaponAbstract
{

    public Transform firePoint;
    public Bullet bulletPrefab;
    public float fireRate = 15f;
    //public float nextTimeToFire = 0f;
    public int numberOfBulletsPerShot;
    public float spread;
    public float speedVariance; // jak duża różnica prędkości wyjściowej pocisku, widoczna przy strzelbie

    public override void Shoot(GameObject shooter, bool isPlayerShooting)
    {
        if (Time.time < nextTimeToFire)
        {
            return;
        }

        nextTimeToFire = Time.time + 1f / fireRate;

        for (int i = 0; i < numberOfBulletsPerShot; i++)
        {
            var randomAngle = Random.Range(-spread / 2, spread / 2);    // spread = 90dg -> odchylenie max o 45dg w lewo i max 45dg w prawo

            var randomSpeed = Random.Range(0, speedVariance);

            var bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation * Quaternion.Euler(0, 0, randomAngle));
            bullet.shooter = shooter;
            bullet.isFromPlayer = isPlayerShooting;
            bullet.speed += randomSpeed;
        }
    }

    public override bool DrawIndicator()
    {
        return false;
    }

    public override void SetAimIndicatorColor(Color colorStart, Color colorEnd)
    {
    }
}
