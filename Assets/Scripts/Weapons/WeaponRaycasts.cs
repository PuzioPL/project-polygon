﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRaycasts : WeaponAbstract
{
    public Transform firePoint;
    public float fireRate = 15f;
    public float impactForce = 7f;
    public float range = 50f;
    public int damage = 10;
    public LineRenderer lineRenderer;
    public LineRenderer aimIndicator;
    public GameObject hitEffect;
    //private float nextTimeToFire = 0f;
    //public bool isLaserIndicator;

    private void Start()
    {
    }

    public override void Shoot(GameObject shooter, bool playerShooting)
    {
        if (Time.time < nextTimeToFire)
        {
            return;
        }

        nextTimeToFire = Time.time + 1f / fireRate;

        int layerMask;
        if (playerShooting)
        {
            // przesunięcie bitowe aby uzyskać maskę dla layera Player
            // efektem będzie maska która będzie przepuszczać jedynie raycasty które uderzą w Playera
            layerMask = 1 << LayerMask.NameToLayer("Player");

            // odwrotność maski powoduje, że raycast będzie patrzył na wszystkie layery, oprócz playera
            layerMask = ~layerMask;
        }
        else
        {
            // to samo ale kiedy Enemy jest strzelcem
            layerMask = 1 << LayerMask.NameToLayer("Enemy");

            layerMask = ~layerMask;
        }

        // rysuje raycast IGNROTUJĄC warstwę player (remember!!)
        RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.up, range, layerMask);

        if (hitInfo)
        {
            Debug.Log(hitInfo.transform.name);
            Actor actor = hitInfo.transform.GetComponent<Actor>();

            if (actor != null)
            {
                actor.TakeDamage(-hitInfo.normal * impactForce, damage);
            }

            // rysuje linię od firePoint do trafionego obiektu
            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, hitInfo.point);

            // -0.5, żeby efekt był PONAD obstacles
            GameObject effect = Instantiate(hitEffect, new Vector3(hitInfo.point.x, hitInfo.point.y, -0.5f), firePoint.rotation * Quaternion.Euler(-90, 90, 90));
            Destroy(effect, 0.5f);
        }
        else
        {
            // rysuje linię od firePoint w przestrzeń (kiedy nic nie trafił)
            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, firePoint.position + firePoint.up * range);
        }

        StartCoroutine(ShowRay());

        if (aimIndicator != null)
        {
            if (aimIndicator.enabled)
            {
                // troche kiepsko z tym ifem ale na razie ujdzie
                aimIndicator.enabled = false;
            }

            Color yellow = new Color(255, 255, 0);
            SetAimIndicatorColor(yellow, yellow);
        }
    }

    IEnumerator ShowRay()
    {
        lineRenderer.enabled = true;

        yield return new WaitForSeconds(0.02f);

        lineRenderer.enabled = false;
    }

    // Poniżej wykorzystuje enemy sniper

    /// <summary>
    /// Returns true if aimed at player
    /// </summary>
    /// <returns></returns>
    public override bool DrawIndicator()
    {

        if (!aimIndicator.enabled)
        {
            // troche kiepsko z tym ifem ale na razie ujdzie
            aimIndicator.enabled = true;
        }

        // shoot raycast here, check if enemy
        // if yes, then SniperState = FirePositionConfirmed

        int layerMask = LayerMask.GetMask("Player", "Obstacle");
        RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.up, range, layerMask);

        if (hitInfo)
        {
            // trafia w obstacle albo player
            aimIndicator.SetPosition(0, firePoint.position);
            aimIndicator.SetPosition(1, hitInfo.point);

            if (hitInfo.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                // przeciwnik namierzony
                return true;
            }
            else
            {
                // przeciwnik znajduje się za przeszkodą -> szukaj dalej
                return false;
            }
        }
        else
        {
            // nie trafia w nic
            aimIndicator.SetPosition(0, firePoint.position);
            aimIndicator.SetPosition(1, firePoint.position + firePoint.up * range);

            return false;
        }
    }

    public override void SetAimIndicatorColor(Color colorStart, Color colorEnd)
    {

        aimIndicator.startColor = colorStart;
        aimIndicator.endColor = colorEnd;
    }
}
