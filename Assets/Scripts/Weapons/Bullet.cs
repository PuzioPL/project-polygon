﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    public GameObject shooter;
    public float lifeTime;
    public float impactForce = 7f;
    public int damage = 10;
    public bool isFromPlayer;
    public Vector2 bulletDirection;
    public GameObject hitEffect;

    void Start()
    {
        rb.velocity = transform.up * speed;
        Invoke("Kill", lifeTime); // aby pociski wystrzelone w przestrzeń nie leciały w nieskończonosć
    }

    private void Kill()
    {
        Destroy(gameObject);
        // Spawnuje effect z taką samą rotacją jak rotacja pocisku, obróconą o (-90, 90, 90) żeby był efekt jakby pocisk "przebijał" cel
        GameObject effect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y, -0.5f), transform.rotation * Quaternion.Euler(-90, 90, 90));
        Destroy(effect, 0.5f);
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        // zarejestrowano uderzenie pocisku w obiekt inny niż strzelający
        if (hitInfo.gameObject != shooter && hitInfo.gameObject.layer == LayerMask.NameToLayer(isFromPlayer ? "Enemy" : "Player"))
        {
            Actor actor = hitInfo.GetComponent<Actor>();
            bulletDirection = transform.up;
            actor.TakeDamage(bulletDirection.normalized * impactForce, damage);

            //Debug.Log(hitInfo.transform.name);
            Kill();
        }

        if (hitInfo.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            Kill();
        }
    }
}
