﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public abstract class WeaponAbstract : MonoBehaviour
{
    public float nextTimeToFire;

    public abstract void SetAimIndicatorColor(Color colorStart, Color colorEnd);
    public abstract void Shoot(GameObject shooter, bool playerShooting);
    public abstract bool DrawIndicator();
    /* // shit be working, przenieś wszystkie wspólne pola dla broni tutaj (jak range, firePoint itd)
    public string yes()
    {
        return "yes";
    }
    */
}
