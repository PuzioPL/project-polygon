﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManagement : MonoBehaviour
{
    public int selectedWeapon = 0;

    public List<WeaponAbstract> availableWeapons;
    public WeaponAbstract currentWeapon;

    void Start()
    {
        // przypisuje bronie do listy availableWeapons, żeby mógł łatwo się przęłączać między nimi
        foreach (Transform weapon in transform)
        {
            availableWeapons.Add(weapon.GetComponent<WeaponAbstract>());
        }

        SelectWeapon();
    }

    private void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            currentWeapon.Shoot(gameObject, true);
        }

        int previousSelectedWeapon = selectedWeapon;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            selectedWeapon = 0;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            selectedWeapon = 1;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            selectedWeapon = 2;
        }

        if (previousSelectedWeapon != selectedWeapon)
        {
            SelectWeapon();
        }
    }

    void SelectWeapon()
    {
        int i = 0;

        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon)
            {
                weapon.gameObject.SetActive(true);
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }

        // zapisuje currentWeapon żebym mógł od razu wywoływać metodę Shoot wybranej broni
        currentWeapon = availableWeapons[selectedWeapon];
    }
}
