﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public LevelSO[] levels;

    public EnemySpawner enemySpawner;
    public int enemiesKilledThisLevel;
    public int currentLevelNumber = 0;

    public TextMeshProUGUI levelText;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        // EnemySpawner: na razie działa, przerobienie tego to low priority
        enemySpawner.currentLevel = levels[currentLevelNumber];
        enemySpawner.currentLevel.SetEnemiesNumber();
        levelText.text = $"Level: {(currentLevelNumber + 1).ToString()} ";
    }

    void Update()
    {
    }

    public void EnemyKilled()
    {
        enemiesKilledThisLevel++;

        // jak na danym poziomie zostali już wszyscy przeciwnicy zespawnieni
        if (enemySpawner.enemiesLeft == 0)
        {
            // to sprawdzaj czy player pokonał wszystkich, żeby przejść do kolejnego poziomu
            if (enemiesKilledThisLevel >= enemySpawner.numberOfallEnemies)
            {
                currentLevelNumber++;
                if (levels.Length > currentLevelNumber)
                {
                    LevelSO currentLevel = levels[currentLevelNumber];
                    enemiesKilledThisLevel = 0;
                    enemySpawner.currentLevel = currentLevel;
                    enemySpawner.numberOfallEnemies = currentLevel.GetNumberOfAllEnemies();
                    enemySpawner.enemiesLeft = enemySpawner.numberOfallEnemies;
                    enemySpawner.nextSpawnTime = Time.time + 3f;
                    enemySpawner.spawnEnemies = true;
                    enemySpawner.currentLevel.SetEnemiesNumber();
                    levelText.text = $"Level: {(currentLevelNumber + 1).ToString()} ";
                    Debug.Log("NEXT LEVEL");
                }
                else
                {
                    levelText.text = "THE END";
                    Debug.Log("THE END");
                }
            }
            else
            {
                Debug.Log("NOT NEXT LEVEL");
            }

        }
    }


}
