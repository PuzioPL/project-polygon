﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairScript : MonoBehaviour
{
    public GameObject crosshair;
    private Vector2 target;
    private Camera cam;

    void Start()
    {
        cam = transform.GetComponent<Camera>();
        Cursor.visible = false;
    }

    void Update()
    {
        target = cam.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        crosshair.transform.position = new Vector3(target.x, target.y, -0.5f);
    }
}
