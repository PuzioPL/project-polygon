﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollowTarget : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 10f;
    private Vector3 velocity = Vector3.zero;
    public Vector3 offset;

    private void LateUpdate()
    {
        Vector3 desiredPosition = target.position;
        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition + offset, ref velocity, smoothTime);
    }
}
