﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyType
{
    public GameObject enemyPrefab;

    public int numberOfEnemies;
    private int numberOfEnemiesLeft;

    /// <summary>
    /// Do ustawienia nubmerOfEnemiesLeft = numberOfEnemies, żeby można było łatwo określić ile zespawnić danego typu przeciwnika na level.
    /// </summary>
    public void InitializeEnemyType()
    {
        numberOfEnemiesLeft = numberOfEnemies;
    }

    public void OneEnemyLess()
    {
        numberOfEnemiesLeft--;
    }

    public int GetNumberOfEnemiesLeft()
    {
        return numberOfEnemiesLeft;
    }
}
