﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public bool spawnEnemies = true;
    public float spawnRadius = 12f;

    public LevelSO currentLevel;

    public float nextSpawnTimeMin = 0.5f;
    public float nextSpawnTimeMax = 2f;
    public float nextSpawnTime = 1f;
    public int numberOfallEnemies;
    public int enemiesLeft;
    public static System.Random rnd;

    void Start()
    {
        numberOfallEnemies = currentLevel.GetNumberOfAllEnemies();
        enemiesLeft = numberOfallEnemies;
        rnd = new System.Random();
    }

    void Update()
    {
        if (!spawnEnemies)
        {
            return;
        }

        // czas na zespawnienie kolejnego przeciwnika
        if (Time.time >= nextSpawnTime && enemiesLeft > 0)
        {
            var enemyTypeWithEnemiesLeft = currentLevel.enemies.Where(p => p.GetNumberOfEnemiesLeft() > 0).ToList();

            if (enemyTypeWithEnemiesLeft.Count < 1)
            {
                spawnEnemies = false;
            }
            else
            {
                int r = rnd.Next(enemyTypeWithEnemiesLeft.Count);

                SpawnEnemy(enemyTypeWithEnemiesLeft[r]);

                nextSpawnTime = Time.time + Random.Range(nextSpawnTimeMin, nextSpawnTimeMax);
            }
        }
    }

    void SpawnEnemy(EnemyType enemyPrefab)
    {
        Vector2 spawnPosition = PlayerController.Position;
        spawnPosition += Random.insideUnitCircle.normalized * spawnRadius;

        Instantiate(enemyPrefab.enemyPrefab, spawnPosition, Quaternion.identity);
        enemiesLeft--;
        enemyPrefab.OneEnemyLess();
    }
}
